#  -*- coding: utf-8 -*-

import cgi
import webapp2
from google.appengine.api import urlfetch
from google.appengine.ext import ndb
import StringIO
import json
import logging
import random
import urllib
import urllib2
import urlparse

# for sending images
from PIL import Image
import multipart



TOKEN = '194175708:AAFBWJLCWXWCC7HzdHJG1RwlIwN3lerS5Sw'

BASE_URL = 'https://api.telegram.org/bot' + TOKEN + '/'

# API_URL = 'http://188.120.233.65/phoneAdvisor.php/?'
# API_URL = 'http://www.praysistence.com/Bot/?'

STATE = None
PICK_UP_PHONE_STEP = None
BRAND = None
USERNAME = None
SONGS = None

cur = None

# ================================

class EnableStatus(ndb.Model):
    # key name: str(chat_id)
    enabled = ndb.BooleanProperty(indexed=False, default=False)


# ================================

def setEnabled(chat_id, yes):
    es = EnableStatus.get_or_insert(str(chat_id))
    es.enabled = yes
    es.put()

def getEnabled(chat_id):
    es = EnableStatus.get_by_id(str(chat_id))
    if es:
        return es.enabled
    return False


# ================================

class MeHandler(webapp2.RequestHandler):
    def get(self):
        urlfetch.set_default_fetch_deadline(60)
        self.response.write(json.dumps(json.load(urllib2.urlopen(BASE_URL + 'getMe'))))


class GetUpdatesHandler(webapp2.RequestHandler):
    def get(self):
        urlfetch.set_default_fetch_deadline(60)
        self.response.write(json.dumps(json.load(urllib2.urlopen(BASE_URL + 'getUpdates'))))


class SetWebhookHandler(webapp2.RequestHandler):
    def get(self):
        urlfetch.set_default_fetch_deadline(60)
        url = self.request.get('url')
        if url:
            self.response.write(json.dumps(json.load(urllib2.urlopen(BASE_URL + 'setWebhook', urllib.urlencode({'url': url})))))


class WebhookHandler(webapp2.RequestHandler):
    def post(self):
        global SONGS
        global BRAND
        global STATE
        global USERNAME
        global PICK_UP_PHONE_STEP
        urlfetch.set_default_fetch_deadline(60)
        body = json.loads(self.request.body)
        logging.info('request body:')
        logging.info(body)
        self.response.write(json.dumps(body))

        # update_id = body['update_id']
        message = body['message']
        message_id = message.get('message_id')
        # date = message.get('date')
        text = message.get('text')
        fr = message.get('from')
        chat = message['chat']
        chat_id = chat['id']

        if not text:
            logging.info('no text')
            return

        def getBrands():
            url = API_URL + 'getBrands=GET'
            response = urllib2.urlopen(url)
            data = json.load(response)
            brands = []
            for brand in data:
                name = brand['Brand']
                brands.append(name)
            logging.info(brands)
            return brands

        def getModels(brand):
            url = API_URL + 'getModels=' + brand
            response = urllib2.urlopen(url)
            data = json.load(response)
            models = []
            for model in data:
                name = model['Model']
                models.append(name)
            return models

        def getUsersPhone(username):
            url = API_URL + 'getUsersPhone=' + username
            # reply(url)
            response = urllib2.urlopen(url)
            data = json.load(response)
            phone = {}
            for v in data:
                phone.update(v[0])
            return phone

        def getCategories():
            url = API_URL + 'getCategories=GET'
            response = urllib2.urlopen(url)
            data = json.load(response)
            categories = []
            for category in data:
                name = category['Name']
                categories.append(name)
            return categories

        def updateUser(username, brand, model):
            url = API_URL + 'updateUser=' + username + '&brand=' + brand + '&model=' + model
            # reply(url)
            encodedUrl = url.replace(' ', '+')
            try:
                response = urllib2.urlopen(encodedUrl)
                reply_keyboard = []
                categories = getCategories()
                for category in categories:
                    reply_keyboard.append([category])
                reply_markup = {'keyboard': reply_keyboard, 'resize_keyboard': True, 'one_time_keyboard': True}
                reply('Gracias', reply_markup=json.dumps(reply_markup))
            except urllib2.URLError as e:
                print e.reason

        def getInstructions(category, brand, model=None):
            url = (API_URL + 'getInstructions=' + category.replace('&', '%26') + '&brand=' + brand + '&model=' + model).replace(' ', '+')
            response = urllib2.urlopen(url)
            data = json.load(response)
            # reply(json.dumps(data))
            if data == []:
                url = (API_URL + 'getInstructions=' + category.replace('&', '%26')+ '&brand=' + brand).replace(' ', '+')
                # reply(url)
                response = urllib2.urlopen(url)
                data = json.load(response)
            # reply(url)
            instructions = {}
            # reply(json.dumps(data))
            if data == []:
                instructions = []
            else:
                for k, v in data.iteritems():
                    instructions.update({k: v})
            return instructions

        def getSongs():
            url = API_URL + 'getSongs=GET'
            response = urllib2.urlopen(url)
            data = json.load(response)
            songs = []
            for song in data:
                songs.append({
                    'Id': song['Id'],
                    'Singer': song['Singer'],
                    'Song': song['Song']
                })
            return songs

        def userVoted(username):
            url = API_URL + 'VotedFor=' + username
            response = urllib2.urlopen(url)
            data = json.load(response)
            if data is None:
                return False
            else:
                return True

        def likeSong(id, username):
            url = API_URL

            values = {'likeSong' : id,
                      'username': username}

            data = urllib.urlencode(values)
            req = urllib2.Request(url, data)
            try:
                response = urllib2.urlopen(req)
                categories = getCategories()
                reply_keyboard = []
                for category in categories:
                    reply_keyboard.append([category])
                reply_markup = {'keyboard': reply_keyboard, 'resize_keyboard': True, 'one_time_keyboard': False}
                reply('Gracias')
            except urllib2.URLError as e:
                reply(e)
            return

        def reply(msg=None, img=None, reply_markup=None):
            if msg and reply_markup:
                resp = urllib2.urlopen(BASE_URL + 'sendMessage', urllib.urlencode({
                    'chat_id': str(chat_id),
                    'text': msg,
                    'reply_markup': reply_markup,
                })).read()
            elif msg:
                resp = urllib2.urlopen(BASE_URL + 'sendMessage', urllib.urlencode({
                    'chat_id': str(chat_id),
                    'text': msg,
                })).read()
            elif img:
                resp = multipart.post_multipart(BASE_URL + 'sendPhoto', [
                    ('chat_id', str(chat_id)),
                    ('reply_to_message_id', str(message_id)),
                ], [
                    ('photo', 'image.jpg', img),
                ])
            else:
                logging.error('no msg or img specified')
                resp = None

            logging.info('send response:')
            logging.info(resp)


        categories = getCategories()
        if text.startswith('/'):
            if text == '/start':
                STATE = ''
                reply_keyboard = []
                brands = getBrands()
                for brand in brands:
                    reply_keyboard.append([brand])
                reply_markup = {'keyboard': reply_keyboard, 'resize_keyboard': True}
                reply('Hola. Haz tu seleccion', reply_markup=json.dumps(reply_markup))
                STATE = 'PICK_UP_PHONE'
                PICK_UP_PHONE_STEP = 1
                setEnabled(chat_id, True)
            elif text == '/stop':
                reply('Bot disabled')
                setEnabled(chat_id, False)
            elif text == '/image':
                img = Image.new('RGB', (512, 512))
                base = random.randint(0, 16777216)
                pixels = [base+i*j for i in range(512) for j in range(512)]  # generate sample image
                img.putdata(pixels)
                output = StringIO.StringIO()
                img.save(output, 'JPEG')
                reply(img=output.getvalue())
            elif text == '/top10':
                try:
                    USERNAME = fr['username']
                except:
                    try:
                        USERNAME = fr['first_name']
                    except:
                        USERNAME = fr['last_name']
                if userVoted(USERNAME):
                    url = API_URL + 'showChart=SHOW'
                    response = urllib2.urlopen(url)
                    data = json.load(response)
                    chart = ''
                    for song in data:
                        chart += song['Singer'] + ' - ' + song['Song'] + ': ' + song['Likes'] + ' Liked\n'
                    categories = getCategories()
                    reply_keyboard = []
                    for category in categories:
                        reply_keyboard.append([category])
                    reply_markup = {'keyboard': reply_keyboard, 'resize_keyboard': True, 'one_time_keyboard': False}
                    reply(chart, reply_markup=json.dumps(reply_markup))
                else:
                    SONGS = getSongs()
                    reply_keyboard = []
                    for song in SONGS:
                        reply_keyboard.append([song['Singer'] + ' - ' + song['Song']])
                    reply_markup = {'keyboard': reply_keyboard, 'resize_keyboard': True, 'one_time_keyboard': True}
                    reply('Selecciona', reply_markup=json.dumps(reply_markup))
                    STATE = 'VOTE_FOR_SONG'
            else:
                reply('What command?')

        # CUSTOMIZE FROM HERE
        elif text in categories:
            category = text
            try:
                USERNAME = fr['username']
            except:
                try:
                    USERNAME = fr['first_name']
                except:
                    USERNAME = fr['last_name']
            phone = getUsersPhone(USERNAME)
            instructions = getInstructions(category, phone['Brand'], phone['Model'])
            if instructions == []:
                reply('Error: No reply data in database')
            else:
                for k, v in instructions.iteritems():
                    reply(k + ': ' + v)
        elif STATE == 'PICK_UP_PHONE':
            if PICK_UP_PHONE_STEP == 1:
                models = getModels(text)
                reply_keyboard = []
                for model in models:
                    reply_keyboard.append([model])
                reply_markup = {'keyboard': reply_keyboard, 'resize_keyboard': True, 'one_time_keyboard': True}
                BRAND = text
                reply('Elige de estas opciones', reply_markup=json.dumps(reply_markup))
            elif PICK_UP_PHONE_STEP == 2:
                model = text
                brand = BRAND
                try:
                    USERNAME = fr['username']
                except:
                    try:
                        USERNAME = fr['first_name']
                    except:
                        USERNAME = fr['last_name']
                updateUser(USERNAME, brand, model)
            elif PICK_UP_PHONE_STEP == 3:
                category = text
                phone = getUsersPhone(USERNAME)
                instructions = getInstructions(category, phone['Brand'], phone['Model'])
                for k, v in instructions.iteritems():
                    reply(k + ': ' + v)
                PICK_UP_PHONE_STEP = 0
                STATE = ''
            PICK_UP_PHONE_STEP = PICK_UP_PHONE_STEP + 1
        elif STATE == 'VOTE_FOR_SONG':
            singer = text.split(' - ')[0]
            songName = text.split(' - ')[1]
            id = -1
            for song in SONGS:
                if song['Singer'] == singer and song['Song'] == songName:
                    id = song['Id']
            likeSong(id, USERNAME)
        elif 'xxx' in text:
            reply('yyy')
        else:
            logging.info('not enabled for chat_id {}'.format(chat_id))


app = webapp2.WSGIApplication([
    ('/me', MeHandler),
    ('/updates', GetUpdatesHandler),
    ('/set_webhook', SetWebhookHandler),
    ('/webhook', WebhookHandler),
], debug=True)
